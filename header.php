<!doctype html>


<html lang="en" class="no-js">
<head>
	<title>Phoenix Innovations</title>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link rel="shortcut icon" href="upload/favicon.ico" type="image/x-icon">
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>


	<link rel="stylesheet" type="text/css" href="css/bootstrap.css" media="screen">	
    <!-- REVOLUTION BANNER CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="css/fullwidth.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="css/settings.css" media="screen" />

	<link rel="stylesheet" type="text/css" href="css/magnific-popup.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" media="screen">
	<!-- <link rel="stylesheet" type="text/css" href="css/font-awesome.css" media="screen"> -->
	<link rel="stylesheet" type="text/css" href="css/animate.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/responsive.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/slick.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/slick-theme.css" media="screen">
	<link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen" charset="utf-8" />
	<link rel="stylesheet" href="css/dropzone.css" type="text/css" media="screen" charset="utf-8" />
	<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
	<link rel="stylesheet" type="text/css" href="css/demo.css" />
    <link rel="stylesheet" type="text/css" href="css/fullsliderstyle.css" />
    <link rel="stylesheet" type="text/css" href="css/custom.css" />
	
	<noscript>
		<link rel="stylesheet" type="text/css" href="css/styleNoJS.css" />
	</noscript>
		
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	<script type="text/javascript" src="js/jquery.migrate.js"></script>
	<script type="text/javascript" src="js/dropzone.js"></script>
	<script src="js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/slick.js"></script>
	<script type="text/javascript" src="js/crawler.js"></script>
	<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
	<script type="text/javascript" src="js/plugins-scroll.js"></script>
	<script type="text/javascript" src="js/waypoint.min.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
	<script type="text/javascript" src="js/jquery.themepunch.revolution.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Noto+Serif" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">
  	<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyDBVn4Riv8vu-wPmReTdVbjDU0Y9nZW8Cs&sensor=false"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script type="text/javascript" src="js/gmap3.min.js"></script>
	
    <!-- jQuery KenBurn Slider  -->
  
	<!--
	##############################
	 - ACTIVATE THE BANNER HERE -
	##############################
	-->
	
	<script type="text/javascript">
	
		var tpj=jQuery;
		tpj.noConflict();
		
		tpj(document).ready(function() {
		var acceptedFileTypes = "application/pdf, .doc, .docx";
		Dropzone.options.dropzone1 = {
			uploadMultiple: false,
			maxFiles: 1,
			acceptedFiles: acceptedFileTypes,
			init: function() {
				this.on("success", function(file, progress) {
					tpj('#uploadcv1 #success1').removeClass('success');
					jQuery("#dropzone1").hide();
					tpj('#error1').hide();
					tpj('a#cls_apply_job1').hide();
				}),
				this.on("error", function(file, errorMessage, xhr) {
					tpj('#error1').removeClass('error');
				})
			}
		}
		
		Dropzone.options.dropzone2 = {
			uploadMultiple: false,
			maxFiles: 1,
			acceptedFiles: acceptedFileTypes,
			init: function() {
				this.on("success", function(file, progress) {
					tpj('#uploadcv2 #success2').removeClass('success');
					jQuery("#dropzone2").hide();
					tpj('#uploadcv2 #error2').hide();
					tpj('a#cls_apply_job2').hide();
				}),
				this.on("error", function(file, errorMessage, xhr) {
					tpj('#uploadcv2 #error2').removeClass('error');
				})
			}
		}
		
		Dropzone.options.dropzone3 = {
			uploadMultiple: false,
			maxFiles: 1,
			acceptedFiles: acceptedFileTypes,
			init: function() {
				this.on("success", function(file, progress) {
					tpj('#uploadcv3 #success3').removeClass('success');
					tpj('#error3').hide();	
					jQuery("#dropzone3").hide();
					tpj('a#cls_apply_job3').hide();
				}),
				this.on("error", function(file, errorMessage, xhr) {
					tpj('#uploadcv3 #error3').removeClass('error');				
				})
			}
		}
		
		tpj('a.cls_apply_job').bind('click',function () {
			var Class = tpj(this).attr('href');
			var ulName = Class;
			var Display=tpj(ulName).css('display');
			var Dis = tpj(ulName).css('display');			
			if (Dis == "block" || Dis == "undefined") {
				tpj('html, body').animate({scrollTop: tpj(this).offset().top - 310}, 1000);
			}
			else {
				tpj('html, body').animate({scrollTop: tpj(this).offset().top - 310}, 1000);
				tpj(ulName).slideDown();
				jQuery(".dropzone .dz-complete").empty();
				jQuery(".dropzone .dz-complete").hide();
				jQuery(".dropzone.dz-started .dz-message").show();
				jQuery("#dropzone4").show();
				tpj('a.cls_apply_job').show();
			}
		});
		});

/* Header resize on scroll with animations */
	function init() {
		window.addEventListener('scroll', function(e){
			var distanceY = window.pageYOffset || document.documentElement.scrollTop,
				shrinkOn = 200,
				header = document.querySelector("#container");
				if (distanceY > shrinkOn) {
					jQuery(header).addClass("smaller");
				} else {
					if (jQuery(header).hasClass("smaller")) {
						jQuery(header).removeClass("smaller");
					}
				}
			});
		}
		
		window.onload = function(e) {
			init();
			tpj(".fullwidthbanner").css('visibility','visible');
			if (tpj.fn.cssOriginal!=undefined)
			tpj.fn.css = tpj.fn.cssOriginal;
			var api = tpj('.fullwidthbanner').revolution(
				{
					delay:8000,
					startwidth:1170,
					startheight:301,
					onHoverStop:"off",						// Stop Banner Timet at Hover on Slide on/off
					thumbWidth:100,							// Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
					thumbHeight:50,
					thumbAmount:3,
					hideThumbs:0,
					navigationType:"none",				// bullet, thumb, none
					navigationArrows:"solo",				// nexttobullets, solo (old name verticalcentered), none
					navigationStyle:"round",				// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom
					navigationHAlign:"center",				// Vertical Align top,center,bottom
					navigationVAlign:"bottom",					// Horizontal Align left,center,right
					navigationHOffset:30,
					navigationVOffset: 40,
					soloArrowLeftHalign:"left",
					soloArrowLeftValign:"center",
					soloArrowLeftHOffset:0,
					soloArrowLeftVOffset:0,
					soloArrowRightHalign:"right",
					soloArrowRightValign:"center",
					soloArrowRightHOffset:0,
					soloArrowRightVOffset:0,
					touchenabled:"on",						// Enable Swipe Function : on/off
					stopAtSlide:-1,							// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
					stopAfterLoops:-1,						// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic
					hideCaptionAtLimit:0,					// It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
					hideAllCaptionAtLilmit:0,				// Hide all The Captions if Width of Browser is less then this value
					hideSliderAtLimit:0,					// Hide the whole slider, and stop also functions if Width of Browser is less than this value
					fullWidth:"on",
					shadow:1								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows -  (No Shadow in Fullwidth Version !)
				});

				// TO HIDE THE ARROWS SEPERATLY FROM THE BULLETS, SOME TRICK HERE:
				// YOU CAN REMOVE IT FROM HERE TILL THE END OF THIS SECTION IF YOU DONT NEED THIS !
					api.bind("revolution.slide.onloaded",function (e) {
						jQuery('.tparrows').each(function() {
							var arrows=jQuery(this);
							var timer = setInterval(function() {
								if (arrows.css('opacity') == 1 && !jQuery('.tp-simpleresponsive').hasClass("mouseisover"))
								  arrows.fadeOut(300);
							},3000);
						})
						jQuery('.tp-simpleresponsive, .tparrows').hover(function() {
							jQuery('.tp-simpleresponsive').addClass("mouseisover");
							jQuery('body').find('.tparrows').each(function() {
								jQuery(this).fadeIn(300);
							});
						}, function() {
							if (!jQuery(this).hasClass("tparrows"))
								jQuery('.tp-simpleresponsive').removeClass("mouseisover");
						})
					});
				// END OF THE SECTION, HIDE MY ARROWS SEPERATLY FROM THE BULLETS
			}
	</script>	
</head>
<body>
<div id = "dialog-1" title = "Success">Success</div>
<div id = "dialog-2" title = "Error">Error</div>
	<!-- Container -->
	<div id="container">
		<!-- Header
		    ================================================== -->
		<header class="clearfix">
			<!-- Static navbar -->
			<div class="navbar navbar-default navbar-fixed-top">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="index.php"><img alt="" src="images/PhoenixLogo.svg" class="cls_logo"></a>
						<span class="cls_logotxt_span">Product Development Focused on Big Data & IoT</span>
						
					</div>
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-right">

							<li><a class="active" href="index.php"><span></span>Home</a>
							</li>
							<li class="drop"><a href="#"><span></span>About Us</a>
								<ul class="drop-down">
									<li><a href="history.php">History</a></li>
									<li><a href="mgmt.php">Management</a></li>
									<li class="careers_mobile"><a href="careers_mobile.php">Careers</a></li>							
									<li class="careers_desk"><a href="careers.php">Careers</a></li>
								</ul>
							</li>
							<li class="drop"><a href="#"><span></span>Products</a>
								<ul class="drop-down">
									<li><a href="blackbox.php">Blackbox</a></li>
									<li><a href="midas.php">Midas</a></li>
								</ul>
							</li>
							<li><a href="contact_information.php"><span></span>Contact Us</a></li>
						</ul>
					</div>
				</div>
			</div>
			
			<!-- Google Analytics -->
			<script>
			  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

			  ga('create', 'UA-105574971-1', 'auto');
			  ga('send', 'pageview');

			</script>
			<!-- End Google Analytics -->
		</header>
		<!-- End Header -->