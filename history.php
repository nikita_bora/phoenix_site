<!-- history.html -->
<?php include('header.php'); ?>
		<!-- content 
			================================================== -->
		<div id="content">
			<ul class="page-tree">
				<li><a href="index.php">Home</a></li>
				<li><a>History</a></li>
			</ul>	
			<!-- Page Banner -->
			<div class="page-banner history-banner">
			<!--<img src="upload/banner/history_banner.jpg"/>-->
				<div class="container">			
				</div>
			</div>

			<h1 class="page-title"><span>Our History</span></h1>

			<div class="contact-box">
				

				<div class="container">
					<div class="row">
						<div class="col-md-12 triggerAnimation animated" data-animate="fadeInRight">
							<div class="contact-info extra_padding">
								<p>In April of 2016, a new entity was born and <strong>"Phoenix Innovations"</strong> was established.</p>
<p>Phoenix Innovations is founded by Mr. Amit Mahajan. Amit was the Founder & Chief Executive Officer of Xcaliber Technologies LLC based out of Atlanta, Georgia in the United states for 7 years which was a venture funded corporation. Xcaliber Technologies had offices in United State and India.</p>

<p>He is the inventor and the brain behind the SmartChk Diagnostics solution. SmartChk has been globally implemented in the United States, Europe, China & India. Blancco Technology Group acquired Xcaliber Technologies LLC and SmartChk product portfolio in March 2016 for an undisclosed amount.</p>

<p>A persistent team of people lead by Mr. Amit Mahajan embarked on a journey. A journey that would eventually develop great Ideas one of which was an innovation in the field of Mobile Diagnostics, an idea that is known as SmartChk.</p>

<p>Phoenix started as a Product development company focused on iOT and Big Data. We have built BigData and iOT products including a patent pending Shipment solution.</p>

<p>Our journey by far has been an interesting one. We’ve had our bit of ups and downs. We have evolved...and will continue to do so in the future.</p>

<p>The contributions, persistent efforts and hard work bring us where we are today. Make us what we are today.</p>

<p>Our journey has just begun! And as our CEO and Founder Mr. Amit Mahajan always says... <strong>"BELIEVE!"</strong>
</p>						
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		<!-- End content -->
		<?php include('footer.php'); ?>	