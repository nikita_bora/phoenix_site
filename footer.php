<!-- footer ================================================== -->
		<footer>
			<div class="up-footer">
				<div class="container">
					<div class="row">

						<div class="col-md-3 triggerAnimation animated" data-animate="fadeInLeft">
							<div class="widget footer-widgets text-widget">
								<div id="footer_bottom">
									<h4>USA</h4>
									<p>950 E State Hwy 114 Suite 160</p><p>Southlake Texas 76092</p>
									<p><br/></p>
									<p>13010 Morris Road Suite 600</p><p>Alpharetta Georgia 30004</p>
									<p><br/></p>
									<p><i class="fa fa-envelope"></i> <a href="mailto:contact@pi108.com">contact@pi108.com</a></p>
								</div>
							</div>
						</div>

						<div class="col-md-3 triggerAnimation animated" id="address2" data-animate="fadeInUp">
							<div class="widget footer-widgets text-widget">
								<div id="footer_bottom">
								<h4>India</h4>
								<p>Xcaliber Infotech Pvt. Ltd.</p>
								<p>A - 103, 1st Floor, ICC Trade Tower,</p><p> Senapati Bapat Road,</p><p>Pune 411016</p><p>Maharashtra, India</p>
								<p><br/></p>
								<!--<p><i class="fa fa-phone"></i> 020-65262684 / 83</p>-->
								<p><i class="fa fa-phone"></i> +91 8007571640</p>
								<p><i class="fa fa-envelope"></i> <a href="mailto:contact@xcaliberinfotech.com">contact@xcaliberinfotech.com</a></p>
								<p><i class="fa fa-link" aria-hidden="true"></i> <a target="blank" href="http://www.xcaliberinfotech.com/">www.xcaliberinfotech.com</a></p>
								</div>
							</div>
						</div>
					<div class="col-md-6" id="footer_menu_links">
						<div class="col-md-6 triggerAnimation animated" data-animate="fadeInDown">
							<div class="widget footer-widgets text-widget">
							<div id="footer_bottom">
								<ul>
								<li><a href="index.php">Home</a></li>
								<li>About Us
									<ul class="cls_submenu">
										<li><a href="history.php">History</a></li>
										<li><a href="mgmt.php">Management</a></li>
										<li class="careers_mobile"><a href="careers_mobile.php">Careers</a></li>							
										<li class="careers_desk"><a href="careers.php">Careers</a></li>
									</ul>
								</li>
								</ul>
							</div>
							</div>
						</div>
						<div class="col-md-6 triggerAnimation animated" data-animate="fadeInRight">
							<div class="widget footer-widgets flickr-widget">
								<div id="footer_bottom">
								<ul>
									<li>Products
										<ul class="cls_submenu">
											<li><a href="blackbox.php">Blackbox</a></li>
											<li><a href="midas.php">Midas</a></li>
										</ul>
									</li>
									<li><a href="contact_information.php">Contact Us</a></li>
								</ul>
							</div>
							</div>
						</div> 
					</div>
					</div>
				</div>
			</div>

			<!--<div class="footer-line">
				<div class="container">
					<p class="cls_footerline">&#169; 2017 Phoenix Innovations LLC. All Rights Reserved</p>
				</div>
			</div>-->
			<div class="footer-line">
				<div class="container">
					<p>&#169; 2017 Phoenix Innovations LLC. All Rights Reserved</p>
					<ul class="footer-social-icons">
						<li><a class="facebook" href="https://www.facebook.com/Phoenix-Innovations-LLC-251080778605662/" target="_blank"><i class="fa fa-facebook"></i></a></li>
						<li><a class="linkedin" href="https://www.linkedin.com/company/22327001/" target="_blank"><i class="fa fa-linkedin"></i></a></li>					
					</ul>
				</div>
			</div>

		</footer>
		<!-- End footer -->
	</div>
	<!-- End Container -->
	<script>
	/* ---------------------------------------------------------------------- */
	/*	Contact Map
	/* ---------------------------------------------------------------------- */
	 var locations = [
      ['<img src="images/loc_logo.png"/><h6 style="font-weight:600;">Development Center</h6><span style="font-weight:400;font-size:12px;">Xcaliber Infotech Pvt. Ltd.<br/>A - 103, 1st Floor, ICC Trade Tower,<br/> Senapati Bapat Road, Pune<br/>Maharashtra, India 411016</span>', 18.535866, 73.829800, 1],
      ['<div style="width:280px;"><div style="float:left;"><img src="images/loc_logo_us.jpg"/></div><div style="float:right; margin-left:10px; margin-right:10px;padding:2px;font-size:12px;"><h6 style="font-weight:600;">Headquarter Office</h6><span style="font-weight:400;">Phoenix Innovations<br/>13010 Morris Road Suite 600 <br/>Alpharetta Georgia<br/>30004</span></div></div>', 34.096611, -84.262489, 2],
	  ['<div style="width:280px;"><div style="float:left;"><img src="images/loc_logo_us.jpg"/></div><div style="float:right; margin-left:10px; margin-right:10px;padding:2px;font-size:12px;"><h6 style="font-weight:600;">Texas Office</h6><span style="font-weight:400;">Phoenix Innovations<br/>950 E State Hwy 114 Suite 160 <br/>Southlake Texas<br/>76092</span></div></div>', 32.955129, -97.136041, 3]
    ];

    var infowindow = new google.maps.InfoWindow();
	var zoom_level = 3;
	if (window.matchMedia('(max-width: 1028px)').matches) {
       zoom_level = 2;
    }
	if (window.matchMedia('(max-width: 414px)').matches) {
		zoom_level = 1;
	}
	
    var marker, i;
	var map = new google.maps.Map(document.getElementById('map'), {
      zoom: zoom_level,
      center: new google.maps.LatLng(37.785620, -6.884536),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
	
	for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
	  google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
        return function() {
          //infowindow.setContent(locations[i][0]);
          infowindow.close(map, marker);
        }
      })(marker, i));
    }
	</script>
	<script type="text/javascript">	
			/* var tpj1=jQuery;
			tpj1.noConflict();
			
			tpj1(function() {
			
				var Page = (function() {

					var $navArrows = tpj1( '#nav-arrows' ),
						$nav = tpj1( '#nav-dots > span' ),
						slitslider = tpj1( '#slider' ).slitslider( {
							onBeforeChange : function( slide, pos ) {

								$nav.removeClass( 'nav-dot-current' );
								$nav.eq( pos ).addClass( 'nav-dot-current' );

							}
						} ),

						init = function() {

							initEvents();
							
						},
						initEvents = function() {

							// add navigation events
							$navArrows.children( ':last' ).on( 'click', function() {

								slitslider.next();
								return false;

							} );

							$navArrows.children( ':first' ).on( 'click', function() {
								
								slitslider.previous();
								return false;

							} );

							$nav.each( function( i ) {
							
								tpj1( this ).on( 'click', function( event ) {
									
									var $dot = tpj1( this );
									
									if( !slitslider.isActive() ) {

										$nav.removeClass( 'nav-dot-current' );
										$dot.addClass( 'nav-dot-current' );
									
									}
									
									slitslider.jump( i + 1 );
									return false;
								
								} );
								
							} );

						};

						return { init : init };

				})();

				Page.init(); */

				/**
				 * Notes: 
				 * 
				 * example how to add items:
				 */

				/*
				
				var $items  = $('<div class="sl-slide sl-slide-color-2" data-orientation="horizontal" data-slice1-rotation="-5" data-slice2-rotation="10" data-slice1-scale="2" data-slice2-scale="1"><div class="sl-slide-inner bg-1"><div class="sl-deco" data-icon="t"></div><h2>some text</h2><blockquote><p>bla bla</p><cite>Margi Clarke</cite></blockquote></div></div>');
				
				// call the plugin's add method
				ss.add($items);

				*/
			
			//});
		</script>
</body>
</html>