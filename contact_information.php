<!-- contact_information -->
<script>
window.onload = function(e){ 
	var jump = window.location.hash.split('/')[0];
	if(jump == '#contact-form'){
		jQuery('html, body').animate({scrollTop: jQuery('#contact-form').offset().top -100 }, 'fast');
	}
}
</script>
<?php include('header.php'); ?>
		<!-- content 
			================================================== -->
		<div id="content">
			<ul class="page-tree">
				<li><a href="index.php">Home</a></li>
				<li><a>Contact us</a></li>
			</ul>
			<!-- Page Banner -->
			<div class="page-banner contact-banner">
				<div class="container">
									
				</div>
			</div>

			<h1 class="page-title"><span>Contact</span></h1>

			<div class="contact-box">
				<div class="title-section">
					<div class="container triggerAnimation animated" data-animate="bounceIn">
						<h1>Get in touch!</h1>
					</div>
				</div>

				<div class="container" >
					<div id="map">						
					</div>
					<div class="row">
						<div class="col-md-12 triggerAnimation animated" data-animate="fadeInLeft">
							<form id="contact-form" name="contact-form">
								<h1>Send us a Message</h1>
								<div class="text-fields">
									<div class="float-input">
										<input name="fname" id="fname" type="text" placeholder="First Name" restricted>
										<span><i class="fa fa-user"></i></span>
									</div>
									<div class="float-input">
										<input name="lname" id="lname" type="text" placeholder="Last Name">
										<span><i class="fa fa-user"></i></span>
									</div>									
									<div class="float-input">
										<input name="mail" id="mail" type="text" placeholder="E-mail">
										<span><i class="fa fa-envelope-o"></i></span>
									</div>						
								</div>

								<div class="text-fields">
									<textarea name="comment" id="comment" placeholder="Message"></textarea>									
								</div>
								<div class="submit-area">
								<div id="msg" class="message"></div>
								<button type="submit" id="submit_contact" class="cls_btn">Send Now</button>
									
								</div>
							</form>
						</div>
						
					</div>
				</div>
			</div>

		</div>
		<!-- End content -->
		<?php include('footer.php'); ?>	