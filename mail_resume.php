<?php
$ds          = DIRECTORY_SEPARATOR;  //1
 
$storeFolder = 'upload/resumes';   //2
 
if (!empty($_FILES)) {
	
	$mailid = "domainp4u@gmail.com";
	$tomailid = "careers@xcaliberinfotech.com";
	$message = "Hello Admin,\n";
	$message .= "Kindly find the attached resume.";
	$subject = "Resume for ".$_POST["profile"]." Profile";
	
	$tempFile    = $_FILES['file']['tmp_name'];
	$file_name        = $_FILES['file']['name'];
	$file_size        = $_FILES['file']['size'];
	$file_type        = $_FILES['file']['type'];
	$file_error       = $_FILES['file']['error'];
	
	if($file_error > 0)
	{
		echo '<div class="error_msg"><i class="fa fa-times-circle-o" aria-hidden="true"></i>Uploading error or No files uploaded. Please try again!</div>';
	}

	//read from the uploaded file & base64_encode content for the mail
	$handle = fopen($tempFile, "r");
	$content = fread($handle, $file_size);
	fclose($handle);
	$encoded_content = chunk_split(base64_encode($content));
	$boundary = md5(uniqid(time()));
	//header
	$headers = "MIME-Version: 1.0\r\n"; 
	$headers .= "From:".$mailid."\r\n"; 
	$headers .= "Reply-To: ".$mailid."\r\n";
	$headers .= "Content-Type: multipart/mixed; boundary = $boundary\r\n\r\n"; 
	//plain text 
	$body = "--$boundary\r\n";
	$body .= "Content-Type: text/plain; charset=ISO-8859-1\r\n";
	$body .= "Content-Transfer-Encoding: base64\r\n\r\n"; 
	$body .= chunk_split(base64_encode($message)); 
	//attachment
	$body .= "--$boundary\r\n";
	$body .="Content-Type: $file_type; name=".$file_name."\r\n";
	$body .="Content-Disposition: attachment; filename=".$file_name."\r\n";
	$body .="Content-Transfer-Encoding: base64\r\n";
	$body .="X-Attachment-Id: ".rand(1000,99999)."\r\n\r\n"; 
	$body .= $encoded_content; 
	$sentMail = @mail($tomailid, $subject, $body, $headers);
	if($sentMail) //output success or failure messages
	{       
		$random=rand(1111,9999);
		$newFileName=$random.$file_name;
		$targetPath = dirname( __FILE__ ) . $ds. $storeFolder . $ds;  //4
		$targetFile =  $targetPath. $newFileName;  //5
		
		if(move_uploaded_file($tempFile,$targetFile)){	
			echo 'success';
		}else{
			echo "error";
		}		
	}else{
		echo 'error';
	}
	
}
?> 