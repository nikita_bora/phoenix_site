<!-- mgmt.html -->
<?php include('header.php'); ?>
<!-- content 
			================================================== -->
		<div id="content">
			<ul class="page-tree">
				<li><a href="index.php">Home</a></li>
				<li><a>Management</a></li>
			</ul>	
			<!-- Page Banner -->
			<div class="page-banner mgmt_banner">
				<div class="container">			
				</div>
			</div>

			<h1 class="page-title"><span>Management</span></h1>

			<div class="single-project">
				<div class="container">
					<div class="row">

						<div class="col-md-12">
							<div class="project-content triggerAnimation animated" data-animate="fadeInUp">
								<div id="mgmt_img">
								<img alt="Amit_Mahajan" src="upload/mgmt/Amit_Mahajan.png" id="">
								<p class="cls_great_vibes">Believe...</p>
								</div>
								<span class="mgmt_desc">
								<h1>Mr. Amit Mahajan</h1>
								<h5>Founder & Chief Executive Officer</h5>
								<p style="text-align:justify">Mr. Amit Mahajan serves as Founder & Chief Executive Officer of  Phoenix Innovations LLC. He has been active in IT industry for last 21 years and has been the driving force behind various large size designs, deliveries & implementations in USA and across the globe.</p>
<p style="text-align:justify">Amit was the Founder & Chief Executive Officer of Xcaliber Technologies LLC based out of Atlanta, Georgia in the United states for 7 years which was a venture funded corporation. Xcaliber Technologies had offices in United State and India.</p>
<p style="text-align:justify">He is the inventor and the brain behind the SmartChk Diagnostics solution. SmartChk has been globally implemented in the United States, Europe, China & India. Blancco Technology Group acquired Xcaliber Technologies LLC and SmartChk product portfolio in March 2016 for an undisclosed amount.</p>
<p style="text-align:justify">Before founding Xcaliber Group which includes Xcaliber Infotech, he held senior leadership roles within multiple Fortune 500 companies, wherein he played responsible role of Chief Architect and Director to name a few.</p>
<p style="text-align:justify">His varied experience, understanding of the solution journey from discovery of problem or need to improvise to reality of having a working solution has been a tremendous asset for Xcaliber in all our initiatives and offerings to customers.</p>
<p style="text-align:justify">His understanding of the complex user experiences of mobile-first consumers and his problem-solving approach has been a driving force. Touted as an “engineer of the future” Amit constantly raises the bar on what it takes to meet society’s growing and demanding mobile needs.</p>
<p style="text-align:justify">Amit is a motorcycle enthusiast. When he is not dreaming up innovative products he is either spending time with his son on Lego Robotics or riding his Ducati XDiavel S in mountains of Georgia.</p>							
								</span>
							</div>
							<div class="project-content triggerAnimation animated" data-animate="fadeInUp">
								<img alt="Ameya_Kandalkar" src="upload/mgmt/amey.jpg" id="mgmt_img">
								<span class="mgmt_desc">
								<h1>Mr. Ameya Kandalkar</h1>
								<h5>Director</h5>
								<p style="text-align:justify">Mr. Ameya Kandalkar serves As Director of Delivery for  Phoenix Innovations LLC since 1st May 2016. Prior to his new appointment, he was part of Xcaliber’s Global implementation team since August 2013 and was closely involved in many of the SmartChk deployments across United States, Europe and Asia Pacific.</p>
								<p style="text-align:justify">Ameya is a highly creative, energetic, customer-focused professional with over 12+ years of progressive experience in various fields of Banking, Consulting & Data Analytics. Immense experience in building and implementing customer focused strategies, and enhancing business functionality with Strong project management and problem solving skills.</p>
								<p style="text-align:justify">One of his previous engagement was with Tata Communications Payment Solutions Ltd. (a fully owned subsidiary of Tata Communications Ltd.) where he has been into Design, architecture and processes framework for Data Analytics/ Reporting – Business Operations. He was also responsible for creating & implementing new data analytics processes in his past assignments.</p>

								<p style="text-align:justify">Ameya is a sports lover. When he is not working on the innovative products he is either spending time with his daughter or will be found playing outdoor sports like Cricket or Badminton.</p>
								</span>
							</div>
							<!--<div class="project-content triggerAnimation animated" data-animate="fadeInUp">
								<img alt="Abhijeet_Barbate" src="upload/mgmt/abhijeetb.jpg" id="mgmt_img">
								<span class="mgmt_desc">
								<h1>Mr. Abhijeet Barbate</h1>
								<h5>Sr. Manager/ Architect</h5>
								<p>Mr. Abhijeet Barbate serves as Sr. Manager/ Architect and is with the organization since October 2011. He was a key product development engineer in development of industry leading mobile diagnostics platform SmartChk.</p>
								<p>Abhijeet is an accomplished and resulted orientated Senior Technical Executive with 10 + years of experience in CRM, Business Intelligence Consulting, Data Analytics. In his entire career span he has worked on product development and services management with blue chip organizations with good analytical and logical skills.</p>
								<p>He received his Bachelors of Engineering in Electronics & Telecommunication from RTM Nagpur University. Additionally, he holds Microsoft certifications. He is regular at contributing and helping with his technology solutions at many user groups over the internet.</p>
								</span>
							</div> -->
						</div>
					</div>

				</div>

			</div>

		</div>
		<!-- End content -->
		<?php include('footer.php'); ?>