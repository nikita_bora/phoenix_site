<!-- Mobile -->
<?php include('header.php'); ?>
		<!-- content 
			================================================== -->
		<div id="content">

			<!-- Page Banner -->
			<ul class="page-tree">
				<li><a>Products</a></li>
				<li><a>	Blackbox</a></li>
			</ul>	
			<div class="page-banner blackbox-banner">
				<div class="container">
				</div>
			</div>

			<h1 class="page-title cls_page_title"><span>Blackbox</span></h1>

			<div class="contact-box">
				
			<div class="container">
				<div class="row">
					<div class="col-md-12 triggerAnimation animated " data-animate="fadeInRight">
						<div class="contact-info ">
							<h1>Supply Chain Analytics:</h1>
							<p><strong>• Why it matters:</strong></p> 
							<div class="cls_inner_spacing"><p>Transform data into real-time, predictive insights Commodity volatility, changing demand forecasts, and supplier-specific challenges have affected nearly every organization—including those with the leading managed supply chains in the world. Even top supply chain performers have faced embarrassing stock-outs during periods of unanticipated demand in recent years. A big reason for this kind of underperformance is the fact that supply chain visibility and analytical models are typically grounded in hindsight. Making decisions based only on what happened in the past no longer provides competitive advantage. </p></div>
							<p><strong>• Potential benefits:</strong></p> 
							<div class="cls_inner_spacing"><p>Insights that make a difference • Use historical enterprise data to feed predictive models that support more informed decisions • Identify hidden inefficiencies to capture greater cost savings • Use risk modeling to conduct “pre-mortems” around significant investments and decisions • Link supply chain models to customer and pricing analytics to clarify the whole profitability picture, not just the parts and pieces.</p></div>
							<p><strong>• What to do now:</strong></p>
							<div class="cls_inner_spacing"><p><strong>a. Treasure hunts</strong> </p>
							<p>Leaders in supply chain performance often use “treasure hunts” to mine data for hidden opportunities. But before you start down that path, you may need to do a bit of data silo busting. That means making sure the information required to drive analytics insights is accessible.</p>
							<p><strong>b. Make more connections</strong></p>
							<p>Focusing on any single link in the supply chain will not deliver the value you’re looking for. High performance requires connecting supply chain forecasting and modeling tools to distribution models, pricing models, and even tax strategies. Only then can you dive deep into specific improvement opportunities such as promotion planning, inventory management, and channel management. The more specific the better.</p>
							</div>

						</div>
					</div>
				</div>
			</div>	
			
			<div class="accord-skills">
				<div class="title-section white">
					<div class="container triggerAnimation animated" data-animate="bounceIn">
						<h1>Areas of focus</h1>
						<p>• Forward Logistics Operations Analytics</p>						
					</div>
				</div>

				<div class="accord-skills-container container">
					<div class="row">
						<div class="col-md-6">
							<div class="accordion-box triggerAnimation animated" data-animate="fadeInLeft">

								<div class="accord-elem">
									<div class="accord-title">
										<h5>Inventory Management</h5>
										<a class="accord-link" href="#"></a>
									</div>
									<div class="accord-content">
										<div class="cls_accord_container">
										<img alt="" src="images/inventory-management-system.png">
										<p>Identify available inventory and keep a tab to plan for additional inventory if needed.</p>
										</div>
									</div>
								</div>								
							</div>
						</div>
						<div class="col-md-6">
						<div class="accordion-box triggerAnimation animated" data-animate="fadeInLeft">
								<div class="accord-elem">
									<div class="accord-title">
										<h5>Delivery (On Time & Accurately)</h5>
										<a class="accord-link" href="#"></a>
									</div>
									<div class="accord-content">
									<div class="cls_accord_container">
										<img alt="" src="images/icon-delivery.png">
										<p>Deliver goods on time to the customer with what was ordered.</p>
									</div>
									</div>
								</div>		
							</div>
						</div>
					</div>
				</div>
				
				<div class="title-section white">
					<div class="container triggerAnimation animated" data-animate="bounceIn">
						<p>• Reverse Logistics Operations Analytics</p>						
					</div>
				</div>

				<div class="accord-skills-container container">
					<div class="row">
						<div class="col-md-6">
							<div class="accordion-box triggerAnimation animated" data-animate="fadeInLeft">

								<div class="accord-elem">
									<div class="accord-title">
										<h5>Disposition cycle time</h5>
										<a class="accord-link" href="#"></a>
									</div>
									<div class="accord-content">
										<div class="cls_accord_container">
										<img alt="" src="images/disposition.png">
										<p>Cycle times can be an important measure of reverse logistics. The more standardized and streamlined the processes are, the shorter the cycle time should be.</p>
										</div>
									</div>
								</div>

								<div class="accord-elem">
									<div class="accord-title">
										<h5>Asset Recovery</h5>
										<a class="accord-link" href="#"></a>
									</div>
									<div class="accord-content">
									<div class="cls_accord_container">
										<img alt="" src="images/assets_recovery.png">
										<p>What percentage of product that moves to the reverse statistics system is reclaimed and resold? How much value is recaptured?</p>
									</div>
									</div>
								</div>

								<div class="accord-elem">
									<div class="accord-title">
										<h5>Remanufacturing/ Refurbishment</h5>
										<a class="accord-link" href="#"></a>
									</div>
									<div class="accord-content">
									<div class="cls_accord_container">
										<img alt="" src="images/refurbished.gif">
										<p>This metric tracks the percentage of product in the reverse logistics stream that is remanufactured/ refurbished in an appropriate manner.</p>
									</div>
									</div>
								</div>
								
								<div class="accord-elem">
									<div class="accord-title">
										<h5>Salvage</h5>
										<a class="accord-link" href="#"></a>
									</div>
									<div class="accord-content">
									<div class="cls_accord_container">
										<img alt="" src="images/salvage.png">
										<p>How much product and other materials are moved to landfills, incinerated, or disposed of as waste? The objective is to minimize product in the waste streams.</p>
									</div>
									</div>
								</div>
								
							</div>
						</div>
						<div class="col-md-6">
						<div class="accordion-box triggerAnimation animated" data-animate="fadeInLeft">

								<div class="accord-elem">
									<div class="accord-title">
										<h5>Maximize Value for assets</h5>
										<a class="accord-link" href="#"></a>
									</div>
									<div class="accord-content">
									<div class="cls_accord_container">
										<img alt="" src="images/assets.png">
										<p>Is the firm maximizing the profitability of product that did not sell well or has been returned by consumers?</p>
									</div>
									</div>
								</div>

								<div class="accord-elem">
									<div class="accord-title">
										<h5>Cost computation</h5>
										<a class="accord-link" href="#"></a>
									</div>
									<div class="accord-content">
									<div class="cls_accord_container">
										<img alt="" src="images/cost_computation.png">
										<p>A cost-per-touch type of metric can be readily computed by dividing total facility costs per month by the number of items processed. This is also a valuable way to compare the efficiencies of different facilities.</p>
									</div>
									</div>
								</div>
								
								<div class="accord-elem">
									<div class="accord-title">
										<h5>Distance traveled</h5>
										<a class="accord-link" href="#"></a>
									</div>
									<div class="accord-content">
									<div class="cls_accord_container">
										<img alt="" src="images/distance_travel.png">
										<p>Tracking average distance traveled per item is not nearly as simple as determining per-item-handling cost. Generally speaking, the fewer miles that can be put on an item in the reverse logistics network, the better.</p>
									</div>
									</div>
								</div>
								
								<div class="accord-elem">
									<div class="accord-title">
										<h5>Total Cost of Ownership</h5>
										<a class="accord-link" href="#"></a>
									</div>
									<div class="accord-content">
									<div class="cls_accord_container">
										<img alt="" src="images/cost.png">
										<p>What is the total cost of ownership related to originally acquiring the product, reselling it, bringing it back as a return, and moving it through a secondary market or placing it in a landfill?</p>
									</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="title-section white">
					<div class="container triggerAnimation animated" data-animate="bounceIn">
						<p>• Product & Process Quality Analytics</p>						
					</div>
				</div>

				<div class="accord-skills-container container">
					<div class="row">
						<div class="col-md-6">
							<div class="accordion-box triggerAnimation animated" data-animate="fadeInLeft">

								<div class="accord-elem">
									<div class="accord-title">
										<h5>Manufacturing Quality Analytics </h5>
										<a class="accord-link" href="#"></a>
									</div>
									<div class="accord-content">
										<div class="cls_accord_container">
										<img alt="" src="upload/iot/analysis.jpg">
										<p>To provide early visibility to quality trends and issues with KPIs, dashboard, alerts via desktop or mobile apps, including traceability of defects back to source - suppliers, plants, work centers, production runs, etc.</p>
										</div>
									</div>
								</div>

								<div class="accord-elem">
									<div class="accord-title">
										<h5>Comprehensive Data Integration </h5>
										<a class="accord-link" href="#"></a>
									</div>
									<div class="accord-content">
									<div class="cls_accord_container">
										<img alt="" src="upload/iot/connectivity.jpg">
										<p>From diverse data sources – both structured and unstructured.</p>
									</div>
									</div>
								</div>								
							</div>
						</div>
						<div class="col-md-6">
						<div class="accordion-box triggerAnimation animated" data-animate="fadeInLeft">
							
								<div class="accord-elem">
									<div class="accord-title">
										<h5>Best Practices</h5>
										<a class="accord-link" href="#"></a>
									</div>
									<div class="accord-content">
									<div class="cls_accord_container">
										<img alt="" src="upload/iot/strategy1.jpg">
										<p>To turn analytics insights into action, avoid common mistakes, achieve faster adoption and improve time to value.</p>
									</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			
							<!-- video bakground section-->
			<div class="video-back">
				<div class="title-section cls_supply_chain_div">
					<div class="container triggerAnimation animated" data-animate="bounceIn">
						<p>Supply chain analytics offers the capability to enable increased automation efficiencies and business intelligence for facilitating more proactive, effective decision making. Businesses can in turn leverage greater economies of scale while more effectively serving the unique and localized market needs of customers.</p>
						<p>Analytics insights can help positively impact top and bottom-line business growth specifically through improvements across the whole supply chain:</p>
					</div>
				</div>

				<!-- video background tags -->
				<div id="customElement"></div>
				<a id="video" class="player" data-property="{videoURL:'https://www.youtube.com/watch?v=bpqhStV2_rc',containment:'#customElement', showControls:false, autoPlay:true, loop:true, mute:true, startAt:0, opacity:1, addRaster:false, quality:'default'}">My video</a> <!--BsekcY04xvQ-->
				<!-- end video background tags -->

				<div class="services-box2 triggerAnimation animated" data-animate="bounceIn">
					<div class="container">
						<div class="row">
							<div class="col-md-4">
								<div class="services-post">
									<a href="#"><i class="fa fa-cubes" aria-hidden="true"></i></a>
									<div class="services-post-content">
										<h4>Design</h4>
										<p>Make better informed, more cost-effective design decisions with time/cost analytics and simulation.</p>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="services-post">
									<a href="#"><i class="fa fa-calendar" aria-hidden="true"></i></a>
									<div class="services-post-content">
										<h4>Planning</h4>
										<p>Prevent overstocking and understocking, achieve higher sales and increase customer satisfaction using demand sensing and forecasting techniques.</p>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="services-post">
									<a href="#"><i class="fa fa-snowflake-o" aria-hidden="true"></i></a>
									<div class="services-post-content">
										<h4>Sourcing</h4>
										<p>Optimize procurement and reduce costs using sourcing analytics for commodity pricing, risk management, spend, supplier performance management and total cost of ownership.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				
				<div class="container">
					<div class="row">
							<div class="col-md-4">
								<div class="services-post">
									<a href="#"><i class="fa fa-cogs" aria-hidden="true"></i></a>
									<div class="services-post-content">
										<h4>Operations</h4>
										<p>Optimize operations, improve process quality and prevent breakdowns with predictive analytics. Streamline & Optimize network flows, reduce costs, maximize value and improve flexibility.</p>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="services-post">
									<a href="#"><i class="fa fa-wrench" aria-hidden="true"></i></a>
									<div class="services-post-content">
										<h4>Service</h4>
										<p>Improve customer service and loyalty with IoT, machine learning and predictive analytics, detection of quality issues via warranty claims analysis, and service network and resource optimization.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
						
						
					</div>
				</div>
			</div>
						
			
			<!-- horizontal tabs -->
			<!--<div class="horizontal-tabs">
				<div class="title-section">
					<div class="container triggerAnimation animated" data-animate="bounceIn">
						<h1>Horizontal Tabs</h1>
						<p>proin gravida nibh vel velit auctor aliquet aenean sollicitudin lorem</p>						
					</div>
				</div>-->

				<!--<div class="horizontal-tabs-box triggerAnimation animated" data-animate="bounceIn">
					<div class="container">
						
						<ul class="nav nav-tabs" id="myMobilePage">
							<li class="active"><a href="#resp-layout-horz" data-toggle="tab"><i class="fa fa-tablet "></i> <span>iPhone App Development</span></a></li>
							<li><a href="#power-admin-horz" data-toggle="tab"><i class="fa fa-mobile"></i> <span>Android App Development</span></a></li>
							<li><a href="#video-support-horz" data-toggle="tab"><i class="fa fa-desktop"></i> <span>UI/UX Design</span></a></li>
							<li><a href="#work-cont-horz" data-toggle="tab"><i class="fa fa-hand-stop-o"></i> <span>Wearable Apps Development</span></a></li>
						</ul>

						<div class="tab-content">
							<div class="tab-pane active" id="resp-layout-horz">
								<img alt="" src="upload/mobile/iphone.jpg">								
								<h3>iPhone App Development</h3>
								<p>Renders high-end iPhone application for different domains that automate the business and ensure top-notch service to the end-users.</p>
							</div>
							<div class="tab-pane" id="power-admin-horz">
								<img alt="" src="upload/mobile/android.jpg">
								<h3>Android App Development</h3>
								<p>We offer complete native app development solutions including customized Android app development that will create unprecedented business value (regarding ROI and developing a loyal user base) for your enterprise and will put you ahead of your business rivals.</p>
							</div>
							<div class="tab-pane" id="video-support-horz">
								<img alt="" src="upload/mobile/uxui.jpg">
								<h3>UI/UX Design</h3>
								<p>We create spell bound designs that are simple and clean yet visually appealing to make striking impact on users. Crafting interactive designing elements for absolute User satisfaction through intelligent interactions between user and product.</p>
							</div>
							<div class="tab-pane" id="work-cont-horz">
							<img alt="" src="upload/mobile/wearable.jpg">
								<h3>Wearable Apps development</h3>
								<p>Every new day knocks the beginning of a new chapter in technology. Wearables are one such example to it. Our team has an impeccable command over the same.</p>
							</div>
							
						</div>
					</div>
				</div> -->
			</div>
			</div>

		</div>
		<!-- End content -->
		<?php include('footer.php'); ?>	