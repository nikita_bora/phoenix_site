<!-- history.html -->
<?php include('header.php'); ?>
		<!-- content 
			================================================== -->
		<div id="content">
			<ul class="page-tree">
				<li><a>Products</a></li>
				<li><a>Midas</a></li>
			</ul>	
			<!-- Page Banner -->
			<div class="page-banner midas-banner">
			<!--<img src="upload/banner/history_banner.jpg"/>-->
				<div class="container">			
				</div>
			</div>

			<h1 class="page-title"><span>Midas</span></h1>

			<div class="contact-box">
				<div class="container">
					<div class="row">
						<div class="col-md-12 triggerAnimation animated " data-animate="fadeInRight">
							<div class="contact-info ">
								<p>In the domain of supply chain, reverse logistics is typically an afterthought. However in the world of hitech electronics and easy consumer returns, reverse logistics plays a key role in unlocking the value of otherwise written-down inventory and saving the retailer millions.</p>
 
								<p>Midas is a very simple to use software suite that enables the lifecycle of the return and efficient disposition while deriving the best possible value for the retailers.</p>
 
								<p>Midas is based on AI enabled inventory routing engine that provides the users with the best value scenario based on multiple parameters.</p>
 
								<p>Midas is a analytics driven solution that enables the reverse logistics operations to determine the appropriate disposition of the product at the point of receiving. The micro services based product allows the receiving process to be integrated with supply chain analytics for returned merchandise, industry specific diagnostics for returned product and state of product warranty and insurance to give an intelligent market value driven disposition of the product.</p>
								<p>With our suites of products in reverse logistics domain, Midas can be tailored and integrated with receiving process across industries to maximize the asset value of the returned product.</p>
								</div>
								</div>
								</div>
								</div>
															
				<div class="container">
				<hr>
					<div class="row">
						<div class="col-md-12 triggerAnimation animated" data-animate="fadeInRight">
							<div class="contact-info">
								&nbsp;
								<h1 style="text-align:center;text-transform:uppercase;">Key Features</h1>
								<p>• Micro services based architecture to plug in different data sources for evaluating the best disposition for a returned product</p>
								<p>• Business rule driven routing that can evaluate multiple parameters, services and data points</p>
								<p>• Responsive UI that can be used by operators on desktops, tablets and phones on the receiving stations or floor for best operational efficiency</p>
								<p>• Prebuilt Mobile industry specific services for warranty checks and cosmetic grading</p>
								<p>• Integration with leading Mobile diagnostics and erasure platform </p>
								<p>• Research and analytics engine to help determine the root cause of the exception and learning over time to handle the exceptions</p>
								<p>• Reports and analytics to provide key performance metrics for operational throughput, exceptions and disposition value</p>
						</div>
						</div>
						</div>
						</div>
						
						<div class="container">
				<hr>
					<div class="row">
						<div class="col-md-12 triggerAnimation animated extra_padding_bottom" data-animate="fadeInRight">
							<div class="contact-info">
								&nbsp;
								<h1 style="text-align:center;text-transform:uppercase;">Key Benefits</h1>
								<p>• Analytics based determination of the disposition bucket is geared to provide the maximize value of the asset in the market. Highly graded product are tagged to return for sale and less desirable products are tagged for scrap or liquidation</p>
								<p>• Higher value of product as the evaluation of the product is moved from a traditional fixed parameter sets to a dynamic analytics based evaluations</p>
								<p>• Reduction in cost of operations of reverse logistics by consolidating the steps for triage and by reducing exceptions</p>
						</div>
						</div>
						</div>
						</div>
													
							</div>
						
					
				

		</div>
		<!-- End content -->
		<?php include('footer.php'); ?>	