<?php include('header.php'); ?>

		<!-- slider -->
		
		<div id="slider" class="revolution-slider">
			<!--
			#################################
				- THEMEPUNCH BANNER -
			#################################
			-->

			<div class="fullwidthbanner-container">
				<div class="fullwidthbanner">
					<ul>
						<!-- THE FIRST SLIDE -->
						<li data-transition="easeFadeOut" data-slotamount="10" data-masterspeed="300">
							<img alt="" src="upload/banner/bigData.jpg" >
							<div class="caption large_text lft"
								 data-x="650"
								 data-y="120"
								 data-speed="600"
								 data-start="1200"
								 data-easing="easeOutExpo" data-end="7000" data-endspeed="300" data-endeasing="easeInSine" ><span class="cls_banner_largetxt cls_banner_largetxt_white" style="">Data is the new science</span></div>
							<div class="caption large_text lft cls_banner_largetxt_white"
								 data-x="650"
								 data-y="170"
								 data-speed="500"
								 data-start="1500"
								 data-easing="easeOutExpo" data-end="7100" data-endspeed="300" data-endeasing="easeInSine" ><span class="cls_banner_smalltxt_white" style="">We help you get answers</span></div>							 
							<div class="caption large_text lft "
								 data-x="650"
								 data-y="200"
								 data-speed="500"
								 data-start="1500"
								 data-easing="easeOutExpo" data-end="7100" data-endspeed="300" data-endeasing="easeInSine" ><span class="cls_banner_smalltxt_white">by asking the right questions</span></div>
						</li>
					<!-- THE second SLIDE -->
						<li data-transition="easeFadeOut" data-slotamount="10" data-masterspeed="300">
							<img alt="" src="upload/banner/midas2.jpg" >
							<div class="caption large_text lft"
								 data-x="20"
								 data-y="130"
								 data-speed="600"
								 data-start="1200"
								 data-easing="easeOutExpo" data-end="7000" data-endspeed="300" data-endeasing="easeInSine" ><span class="cls_banner_largetxt">Turning everyday objects into</span></div>
							<div class="caption large_text lft"
								 data-x="20"
								 data-y="200"
								 data-speed="600"
								 data-start="1200"
								 data-easing="easeOutExpo" data-end="7000" data-endspeed="300" data-endeasing="easeInSine" ><span class="cls_banner_largetxt">data collecting gold</span></div>
							<!--<div class="caption large_text lft"
								 data-x="18"
								 data-y="30"
								 data-speed="600"
								 data-start="1200"
								 data-easing="easeOutExpo" data-end="7000" data-endspeed="300" data-endeasing="easeInSine" ><span class="cls_banner_smalltxt">Design and develop applications</span></div>								 
							<div class="caption large_text lft"
								 data-x="18"
								 data-y="65"
								 data-speed="600"
								 data-start="1200"
								 data-easing="easeOutExpo" data-end="7000" data-endspeed="300" data-endeasing="easeInSine" ><span class="cls_banner_smalltxt">with latest technology</span></div>								 
							<div class="caption lfb"
								 data-x="803"
								 data-y="54"
								 data-speed="600"
								 data-start="1200"
								 data-easing="easeOutExpo" data-end="7000" data-endspeed="300" data-endeasing="easeInSine" ><img src="upload/banner/icon5.png" alt="Image 1"></div>
							<div class="caption modern_medium_light lfl " id="ios_banner"
								 data-x="15"
								 data-y="110"
								 data-speed="500"
								 data-start="1600"
								 data-easing="easeOutExpo" data-end="7100" data-endspeed="300" data-endeasing="easeInSine" ><i class="fa fa-list"></i>IOS App Development</div>
							<div class="caption modern_medium_light lft stt" id="android_banner"
								 data-x="300"
								 data-y="110"
								 data-speed="600"
								 data-start="1900"
								 data-easing="easeOutExpo" data-end="7300" data-endspeed="300" data-endeasing="easeInSine" ><i class="fa fa-building-o"></i>Android App Development</div>
							<div class="caption modern_medium_light lfl stt" id="variable_banner"
								 data-x="15"
								 data-y="230"
								 data-speed="600"
								 data-start="2100"
								 data-easing="easeOutExpo" data-end="7400" data-endspeed="300" data-endeasing="easeInSine" ><i class="fa fa-flag-o"></i>Wearable App Development</div>
							<div class="caption modern_medium_light lfb stt" id="ux_banner"
								 data-x="300"
								 data-y="230"
								 data-speed="600"
								 data-start="2300"
								 data-easing="easeOutExpo" data-end="7500" data-endspeed="300" data-endeasing="easeInSine" ><i class="fa fa-laptop"></i>UI/UX Design</div>-->
						</li>
						
						
						<!-- THE third SLIDE -->
						<li data-transition="easeFadeOut" data-slotamount="10" data-masterspeed="300">
							<!-- THE MAIN IMAGE IN THE third SLIDE -->
							<img alt="" src="upload/banner/supplyChain2.jpg" >

							<div class="caption large_text lft"
								 data-x="600"
								 data-y="140"
								 data-speed="600"
								 data-start="1200"
								 data-easing="easeOutExpo" data-end="7000" data-endspeed="300" data-endeasing="easeInSine"><span class="cls_banner_smalltxt_white">We define, design and execute strategies</span></div>
								 
							<div class="caption large_text lft"
								 data-x="600"
								 data-y="180"
								 data-speed="600"
								 data-start="1200"
								 data-easing="easeOutExpo" data-end="7000" data-endspeed="300" data-endeasing="easeInSine" ><span class="cls_banner_smalltxt_white">that drive your business growth, reduce costs</span></div>

							<div class="caption large_text lft"
								 data-x="600"
								 data-y="220"
								 data-speed="600"
								 data-start="1200"
								 data-easing="easeOutExpo" data-end="7000" data-endspeed="300" data-endeasing="easeInSine" ><span class="cls_banner_smalltxt_white">and create new revenue streams.</span></div>
							
							<!-- THE CAPTIONS IN THIS SLDIE -->
							<!--<div class="caption large_text lft"
								 data-x="530"
								 data-y="120"
								 data-speed="600"
								 data-start="1200"
								 data-easing="easeOutExpo" data-end="7000" data-endspeed="300" data-endeasing="easeInSine" ><span class="cls_banner_largetxt_white">Turning everyday objects into</span></div>
							<div class="caption large_text lft"
								 data-x="530"
								 data-y="190"
								 data-speed="600"
								 data-start="1200"
								 data-easing="easeOutExpo" data-end="7000" data-endspeed="300" data-endeasing="easeInSine" ><span class="cls_banner_largetxt_white">data collecting gold</span></div>-->
						 		 
						</li>
							
						<!-- THE fourth SLIDE -->
						<li data-transition="easeFadeOut" data-slotamount="10" data-masterspeed="300">
							<!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
							<img alt="" src="upload/banner/midas1.jpg" >

							<!-- THE CAPTIONS IN THIS SLDIE -->
							<!--<div class="caption large_text lft"
								 data-x="8"
								 data-y="100"
								 data-speed="600"
								 data-start="1200"
								 data-easing="easeOutExpo" data-end="7000" data-endspeed="300" data-endeasing="easeInSine"><span class="cls_banner_smalltxt_white">We define, design and execute strategies</span></div>
								 
							<div class="caption large_text lft"
								 data-x="8"
								 data-y="140"
								 data-speed="600"
								 data-start="1200"
								 data-easing="easeOutExpo" data-end="7000" data-endspeed="300" data-endeasing="easeInSine" ><span class="cls_banner_smalltxt_white">that drive your business growth, reduce costs</span></div>

							<div class="caption large_text lft"
								 data-x="8"
								 data-y="180"
								 data-speed="600"
								 data-start="1200"
								 data-easing="easeOutExpo" data-end="7000" data-endspeed="300" data-endeasing="easeInSine" ><span class="cls_banner_smalltxt_white">and create new revenue streams.</span></div>
								 
							<div class="caption lfl" id="consul_img"
								 data-x="600"
								 data-y="15"
								 data-speed="600"
								 data-start="1800"
								 data-easing="easeOutExpo" data-end="7200" data-endspeed="300" data-endeasing="easeInSine" ><img src="upload/banner/technology_consulting.png" alt="Image 1" style="width:590px;height:330px;"></div>-->

						</li>
					</ul>
				</div>
			</div>
		</div>
		
		<!-- <div id="slider" class="sl-slider-wrapper demo-1">

				<div class="sl-slider">
				
					<div class="sl-slide bg-1" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
						<div class="sl-slide-inner">
							<div class="deco"><img src="images/smartPhone.png" width="100%"/></div>
							<h2>App Development</h2>
							<blockquote><p>When mobile meets the right application, miracles happen! Design and Develop Apps with Latest Technology.</p><cite></cite></blockquote>
						</div>
					</div>
					
					<div class="sl-slide bg-2" data-orientation="vertical" data-slice1-rotation="10" data-slice2-rotation="-15" data-slice1-scale="1.5" data-slice2-scale="1.5">
						<div class="sl-slide-inner">
							<div class="deco" data-icon="q"></div>
							<h2>Regula aurea</h2>
							<blockquote><p>Until he extends the circle of his compassion to all living things, man will not himself find peace.</p><cite>Albert Schweitzer</cite></blockquote>
						</div>
					</div>
					
					<div class="sl-slide bg-3" data-orientation="horizontal" data-slice1-rotation="3" data-slice2-rotation="3" data-slice1-scale="2" data-slice2-scale="1">
						<div class="sl-slide-inner">
							<div class="deco"><img src="images/smartPhone.png" width="100%"/></div>
							<h2>Dum spiro, spero</h2>
							<blockquote><p>Thousands of people who say they 'love' animals sit down once or twice a day to enjoy the flesh of creatures.</p><cite>Dame Jane Morris Goodall</cite></blockquote>
						</div>
					</div>
					
					<div class="sl-slide bg-4" data-orientation="vertical" data-slice1-rotation="-5" data-slice2-rotation="25" data-slice1-scale="2" data-slice2-scale="1">
						<div class="sl-slide-inner">
							<div class="deco" data-icon="I"></div>
							<h2>Donna nobis pacem</h2>
							<blockquote><p>The human body has no more need for cows' milk than it does for dogs' milk, horses' milk, or giraffes' milk.</p><cite>Michael Klaper M.D.</cite></blockquote>
						</div>
					</div>
					
					<div class="sl-slide bg-5" data-orientation="horizontal" data-slice1-rotation="-5" data-slice2-rotation="10" data-slice1-scale="2" data-slice2-scale="1">
						<div class="sl-slide-inner">
							<div class="deco" data-icon="t"></div>
							<h2>Acta Non Verba</h2>
							<blockquote><p>I think if you want to eat more meat you should kill it yourself and eat it raw so that you are not blinded by the hypocrisy of having it processed for you.</p><cite>Margi Clarke</cite></blockquote>
						</div>
					</div>
				</div>
				
				<nav id="nav-arrows" class="nav-arrows">
					<span class="nav-arrow-prev">Previous</span>
					<span class="nav-arrow-next">Next</span>
				</nav>

				<nav id="nav-dots" class="nav-dots">
					<span class="nav-dot-current"></span>
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</nav>

			</div> -->
		
		<!-- End slider -->

		<!-- content -->
		<div id="content">

			<!-- Services-box1 -->
			 <div class="services-box1">
			 <div class="title-section">
					<div class="container triggerAnimation animated" data-animate="bounceIn">
						<h1>Products</h1>
						<p>Check out our products</p>						
					</div>
				</div>
				<div class="container">
					<div class="row">
					<div class="col-md-2">
					</div>
					<div class="col-md-4">
						<div class="media"> 
							<div class="services-post triggerAnimation animated" data-animate="fadeInLeft">
									<a href="blackbox.php"><img src="images/blackBox.png" /></a>
									<!--<h4>Blackbox</h4>-->
								</div>
						<a href="blackbox.php">	
						<div class="media__body">
							<h2>Blackbox</h2>
							<p>It offers the capability to enable increased automation efficiencies and business intelligence for facilitating more proactive, effective decision making.</p>
						</div>
						</a>
						</div>
					</div>

					 <div class="col-md-4">
						<div class="media"> 
							<div class="services-post triggerAnimation animated" data-animate="fadeInLeft">
									<a href="midas.php"><img src="images/MidasLogo.png" /></a>
									<!--<h4>Midas</h4>-->
								</div>
						<a href="midas.php">	
						<div class="media__body">
							<h2>Midas</h2>
							<p>Our Supply Chain solution for Reverse Logistics, enables companies to manage returns and derive best possible. Most importantly it changes dynamically to your changing business conditions.</p>
						</div>
						</a>
						</div>
					</div>
					
					<div class="col-md-2">
						
					</div> 

					</div>
				</div>
			</div> 

			<!-- Recent works -->
			<div class="technology_logos">
				<div class="title-section">
					<div class="container triggerAnimation animated" data-animate="bounceIn">
						<h1>Technology</h1>
						<p>Check out some of our areas of expertise</p>						
					</div>
				</div>
				<div id="owl-demo" class="owl-carousel owl-theme triggerAnimation animated" data-animate="fadeInUp">
					<div class="marquee" id="mycrawler2">
						<img alt="" src="upload/tech_logos/microsoft.jpg">
						<img alt="" src="upload/tech_logos/hadoop.jpg">
						<img alt="" src="upload/tech_logos/spark.jpg">
						<img alt="" src="upload/tech_logos/hive.jpg">
						<img alt="" src="upload/tech_logos/sqoop.jpg">
						<img alt="" src="upload/tech_logos/pig.jpg">
						<img alt="" src="upload/tech_logos/tableau.jpg">
						<img alt="" src="upload/tech_logos/qlik.jpg">
						<img alt="" src="upload/tech_logos/looker.jpg">
						<img alt="" src="upload/tech_logos/microstrategy.jpg">
						<img alt="" src="upload/tech_logos/android.jpg">
						<img alt="" src="upload/tech_logos/apache.jpg">
						<img alt="" src="upload/tech_logos/java.jpg">
						<img alt="" src="upload/tech_logos/ubuntu.jpg">
						<img alt="" src="upload/tech_logos/iphone.jpg">
						<img alt="" src="upload/tech_logos/amazon.jpg">
						<img alt="" src="upload/tech_logos/cloudera.jpg">
						<img alt="" src="upload/tech_logos/hortonworks.jpg">
						<img alt="" src="upload/tech_logos/drools.jpg">
				</div>
			</div>

			<!-- Choose-tempcore -->
			<!--<div class="choose-xcaliber">
				<div class="title-section">
					<div class="container triggerAnimation animated" data-animate="bounceIn">
						<h1>Why choose Phoenix</h1>
						<div class="cls_highlighter_bold"></div>
						<div class="cls_highlighter_light"></div>				
					</div>
				</div>
				<div class="container triggerAnimation animated" data-animate="bounceIn">
					<div class="row">
						<div class="col-md-6 image-sect">
							<img alt="" src="upload/home/overview.jpg">
						</div>

						<div class="col-md-6 text-sect">			
							<h4>Our People</h4>
							<ul class="tempcore-features">
								<li>
									<span><i class="fa fa-thumbs-up"></i></span>
									<p>All top-tier consulting and industry backgrounds</p>
								</li>
								<li>
									<span><i class="fa fa-thumbs-up"></i></span>
									<p>eXecution focused teams get the job done</p>
								</li>
								<li>
									<span><i class="fa fa-thumbs-up"></i></span>
									<p>Very strong consulting & product development experience</p>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div> -->
		</div>
		<div class="tempcore-line triggerAnimation animated" data-animate="fadeIn">
				<div class="container">
					<p><i class="fa fa-thumbs-up"></i><span>We are a multi domain product development & services company. Our strength is in Innovation, Execution and Deep Domain expertise.</span></p>
				</div>
			</div>
		<!-- End content -->
		<?php include('footer.php'); ?>