<?php 

	/* ==========================  Define variables ========================== */

	#Your e-mail address
	define("__TO__", "contact@xcaliberinfotech.com");

	#Message subject
	define("__SUBJECT__", "Xcaliber Site - Contact Us:");

	#Success message
	define('__SUCCESS_MESSAGE__', "Your message has been sent. Thank you!");

	#Error message 
	define('__ERROR_MESSAGE__', "Error, your message hasn't been sent");

	#Messege when one or more fields are empty
	define('__MESSAGE_EMPTY_FILDS__', "Please fill out  all fields");

	/* ========================  End Define variables ======================== */

	//Send mail function
	function send_mail($to,$subject,$message,$headers){
		if(@mail($to,$subject,$message,$headers)){
			echo json_encode(array('info' => 'success', 'msg' => __SUCCESS_MESSAGE__));
		} else {
			echo json_encode(array('info' => 'error', 'msg' => __ERROR_MESSAGE__));
		}
	}

	//Check e-mail validation
	
	/*function check_email($email){
		if(!@eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email)){
			return false;
		} else {
			return true;
		}
	}*/

	//Get post data
	//if(isset($_POST['fname']) and isset($_POST['mail']) and isset($_POST['comment'])){
		
		$fname 	 = $_POST['fname'];
		$lname 	 = $_POST['lname'];
		$mail 	 = $_POST['mail'];
		$comment = $_POST['comment'];

		$name= $fname . ' ' . $lname;
		
		/*if($fname == '') {
			echo json_encode(array('info' => 'error', 'msg' => "Please enter your first name."));
			exit();
		}else if($lname == '') {
			echo json_encode(array('info' => 'error', 'msg' => "Please enter your last name."));
			exit();
		} else if($mail == '' or check_email($mail) == false){
			echo json_encode(array('info' => 'error', 'msg' => "Please enter valid e-mail."));
			exit();
		} else if($comment == ''){
			echo json_encode(array('info' => 'error', 'msg' => "Please enter your message."));
			exit();
		} else { */
			//Send Mail
			$to = __TO__;
			$subject = __SUBJECT__ . ' ' . $name;
			$message = '
			<html>
			<head>
			  <title>Mail from '. $name .'</title>
			</head>
			<body>
			  <table style="width: 500px; font-family: arial; font-size: 14px;" border="1">
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">E-mail:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $mail .'</td>
				</tr>
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Comment:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $comment .'</td>
				</tr>
			  </table>
			</body>
			</html>
			';

			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
			$headers .= 'From: <contact@xcaliberinfotech.com>' . "\r\n";

			send_mail($to,$subject,$message,$headers);
		/*}*/
	//} else {
	//	echo json_encode(array('info' => 'error', 'msg' => __MESSAGE_EMPTY_FILDS__));
	//}
 ?>