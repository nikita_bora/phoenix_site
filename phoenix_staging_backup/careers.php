<!-- careers -->
<?php include('header.php'); ?>
		<!-- content 
			================================================== -->
		<div id="content">

			<!-- Page Banner -->
			<ul class="page-tree">
				<li><a href="index.php">Home</a></li>
				<li><a>Careers</a></li>
			</ul>	
			<div class="page-banner career-banner">
				<div class="container">
				</div>
			</div>

			<h1 class="page-title"><span>Careers</span></h1>

			<div class="contact-box">
				
				<div class="row" id="row1">
					<div class="col-md-12 triggerAnimation animated" data-animate="fadeInRight">
						<div class="container">
						<div class="col-md-6">
						<img src="upload/career/lookingjob.jpg" />
						</div>
						<div class="col-md-6">
						<div class="cls_jointext">
							<h2>Join the Phoenix Innovations Family</h2>
							<p>Find new exciting opportunities that promises you long term personal and professional development in a team that has high morale, excellent leadership, collaborative and friendly environment.</p>
							<p><a href="job_openings.php" class="cls_btn">Find a Job</a></p>
						</div>
						</div>
						</div>
					</div>
				</div>
				
				<div class="row" id="row2">
					<div class="col-md-12 triggerAnimation animated" data-animate="fadeInRight">
						<div class="container">
						<div class="col-md-6">
						<div class="cls_careertext">
							<h2>Culture</h2>
							<p>Be part of a working culture based on believing in your capabilities, that makes great efforts for excellence and encourages everyone to give their best, aim for the sky and deliver.</p>
						</div>
						</div>
						<div class="col-md-6">
							<img src="upload/career/004.png" />
						</div>
						</div>
					</div>
				</div>
				<div class="row" id="row3">
					<div class="col-md-12 triggerAnimation animated" data-animate="fadeInRight">
						<div class="container">
						<div class="col-md-6">
							<img src="upload/career/003.png" width="500"/>
						</div>
						<div class="col-md-6">
							<div class="cls_careertext">
								<h2>Environment</h2>
								<p>Become one who enjoys our vibrant, fast-paced environment. You will be expected to face professional challenges, while having fun at work. Out-of-the-box thinking is key to a success at Xcaliber.​​</p>
							</div>
						</div>
						</div>
					</div>
				</div>
				<div class="row" id="row4">
					<div class="col-md-12 triggerAnimation animated" data-animate="fadeInRight">
						<div class="container">
						<div class="col-md-6">
						<div class="cls_careertext">
								<h2>Workforce</h2>
								<p>Join some of the industry’s most creative minds who are the backbone of our overall business success.  Our  experienced team of professionals are always encouraged to keep up with the latest technologies and business advances.</p>
							</div>
						</div>
						<div class="col-md-6">
							<img src="upload/career/002.png" width="500"/>
							
						</div>
						</div>
					</div>
				</div>
				<div class="row" id="row4">
					<div class="col-md-12 triggerAnimation animated" data-animate="fadeInRight">
						<div class="container">
						<div class="col-md-6">
							<img src="upload/career/001.png" width="500"/>	
						</div>							
						<div class="col-md-6">
							<div class="cls_careertext">
								<h2>Innovative Leadership</h2>
								<p>Our passion for cutting edge technology. Innovation is a catalyst for the growth and success of our business, and help to adapt and grow in the marketplace.​</p>
							</div>
						</div>
						</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		<!-- End content -->
		<?php include('footer.php'); ?>	