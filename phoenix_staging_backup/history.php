<!-- history.html -->
<?php include('header.php'); ?>
		<!-- content 
			================================================== -->
		<div id="content">
			<ul class="page-tree">
				<li><a href="index.php">Home</a></li>
				<li><a>History</a></li>
			</ul>	
			<!-- Page Banner -->
			<div class="page-banner history-banner">
			<!--<img src="upload/banner/history_banner.jpg"/>-->
				<div class="container">			
				</div>
			</div>

			<h1 class="page-title"><span>Our History</span></h1>

			<div class="contact-box">
				

				<div class="container">
					<div class="row">
						<div class="col-md-12 triggerAnimation animated" data-animate="fadeInRight">
							<div class="contact-info extra_padding">
								<p>It’s 3rd December... </p>
								<p>This day in 2008, a new entity was born. On 3rd December 2008 <strong>“Phoenix Innovations”</strong> was incorporated.</p>
								<p>A persistent team of people lead by Mr. Amit Mahajan embarked on a journey. A journey that would eventually develop great Ideas one of which was an innovation in the field of Mobile Diagnostics, an idea that is known as SmartChk.</p> 
								<p>Phoenix Innovations started off as a consulting business with just a few enthusiastic people. Over the years we’ve grown. We have on board people who put their faith in us. Today we have a great team... a team that is full of confidence and capable of reaching great heights.</p>
								<p>It’s been great Years... </p>
								<p>Our journey by far has been an interesting one. We’ve had our bit of ups and downs. We have evolved...and will continue to do so in the future.</p>
								<p>The contributions, persistent efforts and hard work bring us where we are today. Make us what we are today.</p>
								<p>Our journey has just begun! And as our CEO and Founder Mr. Amit Mahajan always says... <strong>“BELIEVE!”</strong></p>						
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		<!-- End content -->
		<?php include('footer.php'); ?>	