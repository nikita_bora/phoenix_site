<!-- job openings -->
<?php include('header.php');  
?>
		<!-- content 
			================================================== -->
		<div id="content">
			<ul class="page-tree">
				<li><a href="index.php">Home</a></li>
				<li><a>Job Openings</a></li>
			</ul>
			<!-- Page Banner -->
			<div class="page-banner job-banner">
				<div class="container">
									
				</div>
			</div>

			<h1 class="page-title"><span>Current Openings</span></h1>
<div class="contact-box">
	<div class="container">
	<div class="row">
		<div class="col-md-12 triggerAnimation animated" data-animate="fadeInRight">
			<div class="contact-info">
			<div class="extra_padding cls_job_section">
				<!--<div class="col-md-12"><h1>There are currently no vacancies available.</h1></div>-->
				<div class="col-md-12">
				<div class="cls_job_listing">
					<h1>Image Processing Experts</h1>
					<p>Experience : <span>3-5 yrs.</span></p>
					<p>Qualification : <span>BE / B.TECH</span></p>
					<p>Location : <span>Pune</span></p>
					<div class="accord-skills-container container" id="id_job_opening">
					<div class="row">
					
						<div class="accordion-box triggerAnimation animated" data-animate="fadeInLeft">
							<div class="accord-elem">
								<div class="accord-title">
									<h5>Job Details :</h5>
									<a class="accord-link" href="#"></a>
								</div>
								<div class="accord-content">
									<div class="cls_accord_container">
										<ul>
										<li>Strong C/C++ background with 3-5 years' experience in Image Processing</li>
										<li>Experience in Algorithm development</li>
										<li>Experience in Matlab, Open CV</li>
										<li>Image Analysis, Computer Vision Algorithm Development</li>
										<li>Experience in Machine Vision techniques</li>
										<li>Must have good verbal and written communication skills</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					
					</div>
					</div>
					<p><a href="#uploadcv1" class="cls_apply_job" id="cls_apply_job1">Apply Job</a></p>
						<div id="uploadcv1" class="upload_cv">
						<form action="mail_resume.php" class="dropzone" id="dropzone1" enctype="multipart/form-data">
						<div class="dz-message" data-dz-message><span>Drop file / Click here to upload<br/>[Supported file format: PDF, DOC, DOCX]</span></div>
						<input type="hidden" name="profile" value="Image Processing Experts">
						</form>
						<div class="success alert alert-success" id="success1">
						<strong><i class="fa fa-check-circle-o" aria-hidden="true"></i></strong> Thank you for showing your interest. We will get back to you shortly!
						</div>
						<div class="error alert alert-danger" id="error1">
						<strong><i class="fa fa-times-circle-o" aria-hidden="true"></i></strong> Oops! There must be some problem. Kindly try again!
						</div>
						
						</div>
				</div>
				
				</div> 
				<!--<div class="col-md-12">
				<div class="cls_job_listing">
					<h1>PHP Developer</h1>
					<p>No. of vacancies : <span>1</span></p>
					<p>Experience : <span>0-3yrs</span></p>
					<p>Qualification : <span>BE / B.TECH</span></p>
					<div class="accord-skills-container container" id="id_job_opening">
					<div class="row">
					
						<div class="accordion-box triggerAnimation animated" data-animate="fadeInLeft">
							<div class="accord-elem">
								<div class="accord-title">
									<h5>Job Details :</h5>
									<a class="accord-link" href="#"></a>
								</div>
								<div class="accord-content">
									<div class="cls_accord_container">
										<ul>
										<li>The role requires a good knowledge of Algorithmic Design and Architecture, Data structures, OOPS Concepts, server less 
        architectures and complex problem solving skills</li>
										<li>Develop, test and deploy web apps in PHP and MySQL using MVC Architecture</li>
										<li>Work with PHP frameworks such as Laravel</li>
										<li>Integrate UI developed using HTML5 / CSS3 into MVC Applications</li>
										<li>Work with Bootstrap and/or Kendo UI</li>
										<li>Work with Javascript, jQuery, AngularJS, AJAX, Web Services(JSON/XML)</li>
										<li>Develop RESTful Web Services</li>
										<li>Source Control Using git and Subversion</li>
										<li>Use Agile or Iterative Development Models </li>
										<li>CDAC certification will be an added advantage</li>
										<li>Must have good verbal and written communication skills</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					
					</div>
					</div>
					<p><a href="#uploadcv2" class="cls_apply_job" id="cls_apply_job2">Apply Job</a></p>
						<div id="uploadcv2" class="upload_cv">
							<form action="mail_resume.php" class="dropzone" id="dropzone2" enctype="multipart/form-data">
							<div class="dz-message" data-dz-message><span>Drop file / Click here to upload<br/>[Supported file format: PDF, DOC, DOCX]</span></div>
							<input type="hidden" name="profile" value="PHP Developer">
							</form>
						<div class="success alert alert-success" id="success2">
						<strong><i class="fa fa-check-circle-o" aria-hidden="true"></i></strong> Thank you for showing your interest. We will get back to you shortly!
						</div>
						<div class="error alert alert-danger" id="error2">
						<strong><i class="fa fa-times-circle-o" aria-hidden="true"></i></strong> Oops! There must be some problem. Kindly try again!
						</div>
						
						</div>
				</div>
				
				</div>
				<div class="col-md-12">
				<div class="cls_job_listing">
					<h1>Java Developer</h1>
					<p>No. of vacancies : <span>1</span></p>
					<p>Experience : <span>0-1yr</span></p>
					<p>Qualification : <span>BE / B.TECH</span></p>
					<div class="accord-skills-container container" id="id_job_opening">
					<div class="row">
					
						<div class="accordion-box triggerAnimation animated" data-animate="fadeInLeft">
							<div class="accord-elem">
								<div class="accord-title">
									<h5>Job Details :</h5>
									<a class="accord-link" href="#"></a>
								</div>
								<div class="accord-content">
									<div class="cls_accord_container">
										<ul>
										<li>Strong understanding of Object Oriented Concepts</li>
										<li>Extensive experience with one of the following - J2EE/ Spring Boot/ Hibernate</li>
										<li>Experience with HTML5, CSS3, Javascript</li>
										<li>Analytical and Problem solving skills</li>
										<li>CDAC certification will be an added advantage</li>
										<li>Must have good verbal and written communication skills</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					
					</div>
					</div>
					<p><a href="#uploadcv3" class="cls_apply_job" id="cls_apply_job3">Apply Job</a></p>
						<div id="uploadcv3" class="upload_cv">
							<form action="mail_resume.php" class="dropzone" id="dropzone3" enctype="multipart/form-data">
							<div class="dz-message" data-dz-message><span>Drop file / Click here to upload<br/>[Supported file format: PDF, DOC, DOCX]</span></div>
							<input type="hidden" name="profile" value="Java Developer">
							</form>
						<div class="success alert alert-success" id="success3">
						<strong><i class="fa fa-check-circle-o" aria-hidden="true"></i></strong> Thank you for showing your interest. We will get back to you shortly!
						</div>
						<div class="error alert alert-danger" id="error3">
						<strong><i class="fa fa-times-circle-o" aria-hidden="true"></i></strong> Oops! There must be some problem. Kindly try again!
						</div>
						
						</div>
				</div>
				
				</div> 
				<div class="col-md-12">
				<div class="cls_job_listing">
					<h1>Big Data</h1>
					<p>No. of vacancies : <span>2</span></p>
					<p>Experience : <span>0-1yr</span></p>
					<p>Qualification : <span>BE / B.TECH</span></p>
					<div class="accord-skills-container container" id="id_job_opening">
					<div class="row">
					
						<div class="accordion-box triggerAnimation animated" data-animate="fadeInLeft">
							<div class="accord-elem">
								<div class="accord-title">
									<h5>Job Details :</h5>
									<a class="accord-link" href="#"></a>
								</div>
								<div class="accord-content">
									<div class="cls_accord_container">
										<ul>
										<li>Product Engineering (Microsoft, JAVA and Open Source)</li>
										<li>Data Engineering (BI, Analytics and Big Data)</li>
										<li>Mobility & Cloud</li>
										<li>Strong knowledge about OOPs and Data Structures</li>
										<li>Analytical and Problem solving skills</li>
										<li>CDAC certification will be an added advantage</li>
										<li>Must have good verbal and written communication skills</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					</div>
					<p><a href="#uploadcv4" class="cls_apply_job" id="cls_apply_job4" rel="">Apply Job</a></p>
						<div id="uploadcv4" class="upload_cv">
							<form action="mail_resume.php" class="dropzone" id="dropzone4" enctype="multipart/form-data">	
							<div class="dz-message" data-dz-message><span>Drop file / Click here to upload<br/>[Supported file format: PDF, DOC, DOCX]</span></div>
							<input type="hidden" name="profile" value="Big Data">
							</form>
						<div class="success alert alert-success" id="success4">
						<strong><i class="fa fa-check-circle-o" aria-hidden="true"></i></strong> Thank you for showing your interest. We will get back to you shortly!
						</div>
						<div class="error alert alert-danger" id="error4">
						<strong><i class="fa fa-times-circle-o" aria-hidden="true"></i></strong> Oops! There must be some problem. Kindly try again!
						</div>
						</div>
				</div>
				
				</div> -->
				<div class="col-md-12">
					<p>However, we are always keen to meet energetic and talented professionals who would like to join our team.</p>
					<p>If you wish to be considered for any future positions, please send your CV and covering letter to :
					<a href="mailto:careers@xcaliberinfotech.com">careers@xcaliberinfotech.com</a> </p>	
				</div> 
			</div>
			</div>
		</div>
	</div>
	</div>
</div>
		<!-- End content -->
		<?php include('footer.php'); ?>	


    